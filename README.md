# What is this project?
This project was developed for hands-on experience in streaming systems. The idea behind it is that with the Twitter-API tweets are stored cleanly on our servers and a sentiment analysis is performed on the text data of the tweets.

# How to get started
* Generate an ssh-key and put the key into realtime-streaming-tweet-sentiments/deployment/inventories/setup/ named as "key"
* Generate Twitter consumer and access tokens and secrets and put them into realtime-streaming-tweet-sentiments/src/main/python/batchingest_for_all_data.py
* Edit Ansible Hosts-IP realtime-streaming-tweet-sentiments/deployment/inventories/hosts.yml to match your Nodes
* Start realtime-streaming-tweet-sentiments/deployment/playbooks/main.yml (pay attention to hosts.yml's groupvar "ansible_ssh_common_args")
* Start realtime-streaming-tweet-sentiments/src/main/scala/RawTweets.scala as spark-submit job. Set consumer and access tokens and se
crets as params for streaming Tweets into the system. See the example below:

```bash
/opt/spark/bin/spark-submit --class RawTweets ~/rbd-semesterarbeit-1.0-SNAPSHOT.jar <consumerKey> <consumerSecret> <accessToken> <accessTokenSecret> follow 25073877 rbd-4:9092,rbd-5:9092,rbd-6:9092
```
