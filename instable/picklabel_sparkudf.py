# TODO:
# - user timestamp parsing
# - entity
# - tweet reply_id, quote_id, retweet_id
# - datatypes -> for example id_str to int instead of string
# - since we extract the retweets we may introduce duplicates -> deduplicate
# - output/sink

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
import json
from keras.preprocessing.text import Tokenizer
from keras.preprocessing import sequence
import keras
import pickle

from keras.models import load_model

# Credit to Zach Moshe: http://zachmoshe.com/2017/04/03/pickling-keras-models.html
import types
import tempfile
import keras.models

def make_keras_picklable():
    def __getstate__(self):
        model_str = ""
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            keras.models.save_model(self, fd.name, overwrite=True)
            model_str = fd.read()
        d = { 'model_str': model_str }
        return d

    def __setstate__(self, state):
        with tempfile.NamedTemporaryFile(suffix='.hdf5', delete=True) as fd:
            fd.write(state['model_str'])
            fd.flush()
            model = keras.models.load_model(fd.name)
        self.__dict__ = model.__dict__


    cls = keras.models.Model
    cls.__getstate__ = __getstate__
    cls.__setstate__ = __setstate__

make_keras_picklable()

model = keras.models.load_model("model/cnn_sentiment.h5")

with open('tokenizer2.pickle', 'rb') as handle:
    imdb_tokenizer = pickle.load(handle)

def get_prediction(review):
    make_keras_picklable()
    review_np_array = imdb_tokenizer.value.texts_to_sequences([review])
    review_np_array = sequence.pad_sequences(review_np_array, maxlen=90, padding="post", value=0)
    score = model.value.predict(review_np_array)[0][0]
    return score


parse = udf(lambda s: json.loads(s, object_hook=json.dumps), ArrayType(StringType()))
predict = udf(lambda p: get_prediction(p), FloatType())

def create_entity_df(entity, data):
    make_keras_pickable()
    return entities.select("tweet_id", entity) \
    .withColumn(entity, explode(parse(col(entity)))) \
    .withColumn("type", lit(entity)) \
    .withColumn("data", get_json_object(col(entity), "$." + data)) \
    .drop(entity)

spark = SparkSession \
    .builder \
    .appName("test") \
    .getOrCreate()

model = spark.sparkContext.broadcast(model)
imdb_tokenizer = spark.sparkContext.broadcast(imdb_tokenizer)

df = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", "rbd-4:9092") \
    .option("subscribe", "raw-track-sanders") \
    .option("startingOffsets", "earliest") \
    .load() \
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")

retweets = df.withColumn("value", get_json_object(col("value"), "$.retweeted_status")) \
    .dropna(subset="value") \
    .withColumn("active", lit(False))

quotes = df.withColumn("value", get_json_object(col("value"), "$.quoted_status")) \
    .dropna(subset="value") \
    .withColumn("active", lit(False))

tweet = df.withColumn("active", lit(True)) \
    .union(retweets).union(quotes).withColumn("tweet_id", get_json_object(col("value"), "$.id_str").cast("long")) \
    .withColumn("text", get_json_object(col("value"), "$.text")) \
    .withColumn("full_text", get_json_object(col("value"), "$.extended_tweet.full_text")) \
    .withColumn("text", coalesce(col("full_text"), col("text"))) \
    .withColumn("emotion", predict(col("text"))) \
    .drop("key", "value", "full_text", "active")
    
tweet_query = tweet \
    .writeStream \
    .format("console") \
    .start()

tweet_query.awaitTermination()
