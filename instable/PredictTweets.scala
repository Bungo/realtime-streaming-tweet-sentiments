import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, get_json_object, udf}
import org.deeplearning4j.nn.modelimport.keras.KerasModelImport
import org.deeplearning4j.nn.modelimport.keras.preprocessing.text.KerasTokenizer
import org.nd4j.linalg.factory.Nd4j

object PredictTweets {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("PredictTweets")
      .getOrCreate()

    spark.conf.set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")


    val tokenizer = KerasTokenizer.fromJson("tokenizer.json")
    val model = KerasModelImport.importKerasSequentialModelAndWeights("cnn_sentiment.h5")

    val predict = udf((input: String) => {
      val initial = tokenizer.textsToSequences(Array(input))
      val padded = Array.fill[Int](initial.length, 90)(0)
      for (i <- 0 until initial.length) {
        for (j <- initial(i).indices) {
          padded(i)(j) = initial(i)(j)
        }
      }
      val prediction = model.output(Nd4j.createFromArray(padded))
      val emotion = prediction.getDouble(0L, 0L)
      emotion
    })

    val df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", "rbd-4:9092")
      .option("subscribe", "staging")
      .option("startingOffsets", "earliest")
      .load()
      .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      .withColumn("text", get_json_object(col("value"), "$.text"))
      .withColumn("emotion", predict(col("text")))

    val query = df
      .writeStream
      .format("console")
      .start()

    query.awaitTermination()

    val input = Array("I am good", "You are bad")
    val initial = tokenizer.textsToSequences(input)
    val padded = Array.fill[Int](initial.length, 90)(0)
    for (i <- 0 until initial.length) {
      for (j <- initial(i).indices) {
        padded(i)(j) = initial(i)(j)
      }
    }

    val output = model.output(Nd4j.createFromArray(padded))
    println(output)
  }
}
