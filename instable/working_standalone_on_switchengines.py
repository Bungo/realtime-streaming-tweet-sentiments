from keras.preprocessing.text import Tokenizer
from keras.preprocessing import sequence
import keras
import pickle

model = keras.models.load_model("cnn_sentiment")
with open('tokenizer2.pickle', 'rb') as handle:
    imdb_tokenizer = pickle.load(handle)
    print(imdb_tokenizer.to_json())

def get_prediction(review):
    review_np_array = imdb_tokenizer.texts_to_sequences([review])
    review_np_array = sequence.pad_sequences(review_np_array, maxlen=90, padding="post", value=0)
    score = model.predict(review_np_array)[0][0]
    return score

print(get_prediction("This is good"))
