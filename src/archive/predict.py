from pyspark.ml import PipelineModel
from pyspark.sql import SparkSession
from pyspark.sql.functions import *


spark = SparkSession \
    .builder \
    .appName("train") \
    .getOrCreate()

df = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", "rbd-4:9092") \
    .option("subscribe", "staging") \
    .option("startingOffsets", "earliest") \
    .load() \
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)") \
    .withColumn("text", get_json_object(col("value"), "$.text")) \

pipelineModel = PipelineModel.load("../../../models/pipelineFit")
df = pipelineModel.transform(df)

query = df \
    .writeStream \
    .format("console") \
    .start()

query.awaitTermination()
