import click
import os
import tweepy

from dotenv import load_dotenv


def authenticate():
    load_dotenv()

    consumer_key = os.getenv('consumer_key')
    consumer_secret = os.getenv('consumer_secret')
    access_token = os.getenv('access_token')
    access_token_secret = os.getenv('access_token_secret')

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    return tweepy.API(auth)


class OptionRequiredIf(click.Option):
    """
    Option is required if the context has `option` set to `value`
    https://stackoverflow.com/questions/46440950/require-and-option-only-if-a-choice-is-made-when-using-click
    """

    def __init__(self, *a, **k):
        try:
            option = k.pop('option')
            value = k.pop('value')
        except KeyError:
            raise(KeyError("OptionRequiredIf needs the option and value "
                           "keywords arguments"))

        click.Option.__init__(self, *a, **k)
        self._option = option
        self._value = value

    def full_process_value(self, ctx, value):
        value = super(OptionRequiredIf, self).full_process_value(ctx, value)
        if value is None and ctx.params[self._option] == self._value:
            msg = 'Required if --{}={}'.format(self._option, self._value)
            raise click.MissingParameter(ctx=ctx, param=self, message=msg)
        return value
