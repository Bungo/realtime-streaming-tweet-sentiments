import click
import json
import time
import tweepy

from util import authenticate


@click.command()
@click.option('--output_file', required=True, type=str)
@click.option('--duration', required=True, type=float)
@click.option('--follow', type=str, default='')
@click.option('--track', type=str, default='*')
def sample(output_file, duration, track, follow):
    api = authenticate()

    class Listener(tweepy.StreamListener):
        ts = time.time()
        first = True

        def on_status(self, status):
            new_ts = time.time()

            if(new_ts < self.ts + duration):
                sep = ","
                if self.first:
                    self.first = False
                    sep = ""
                with open(output_file, 'a') as f:
                    f.write(sep)
                    json.dump(status._json, f)
                return True
            else:
                return False

    stream = tweepy.Stream(auth=api.auth, listener=Listener())
    stream.filter(track=[track], follow=[follow])


if __name__ == "__main__":
    sample()
