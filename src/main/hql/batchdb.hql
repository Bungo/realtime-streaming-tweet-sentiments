CREATE DATABASE datawarehouse;

USE datawarehouse;

CREATE EXTERNAL TABLE IF NOT EXISTS Tweet (
  tweet_id bigint,
  author_id bigint,
  reply_id bigint,
  quote_id bigint,
  retweet_id bigint,
  `timestamp` bigint,
  text string,
  source string,
  retweet_count int,
  lang string,
  adressed_at string,
  emotion double,
  afinn_sentiment float,
  active boolean
) PARTITIONED BY (`date` date) STORED AS PARQUET LOCATION 'hdfs://rbd-7:9000/datawarehouse/tweets';

MSCK REPAIR TABLE Tweet;

CREATE EXTERNAL TABLE IF NOT EXISTS `User` (
  user_id bigint,
  name string,
  screen_name string,
  location string,
  `timestamp` bigint
) STORED AS PARQUET LOCATION 'hdfs://rbd-7:9000/datawarehouse/users/';


CREATE EXTERNAL TABLE IF NOT EXISTS Entity (
  tweet_id bigint, type string, data string
) STORED AS PARQUET LOCATION 'hdfs://rbd-7:9000/datawarehouse/entities/';

CREATE TABLE Visualization AS
SELECT adressed_at, emotion, `date`, count(DISTINCT(tweet_id)) AS count FROM Tweet
WHERE active = True
GROUP BY adressed_at, emotion, `date`;

