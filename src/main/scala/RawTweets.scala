import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import twitter4j.FilterQuery

object RawTweets {
  def main(args: Array[String]): Unit = {
    if (args.length < 7 || args.length % 7 != 0) {
      System.err.println("Usage: RawTweets <consumer key> <consumer secret> " +
        "<access token> <access token secret> " +
        "<follow/track> <id/word> <kafka.bootstrap.servers>")
      System.exit(1)
    }

    val Array(consumerKey, consumerSecret, accessToken, accessTokenSecret) = args.take(4)
    System.setProperty("twitter4j.oauth.consumerKey", consumerKey)
    System.setProperty("twitter4j.oauth.consumerSecret", consumerSecret)
    System.setProperty("twitter4j.oauth.accessToken", accessToken)
    System.setProperty("twitter4j.oauth.accessTokenSecret", accessTokenSecret)

    val filterType = args(4)
    if (filterType != "follow" && filterType != "track") {
      System.err.println("Usage: RawTweets <consumer key> <consumer secret> " +
        "<access token> <access token secret> " +
        "<follow/track> <id/word>")
      System.err.println("filterType must be follow or track")
      System.exit(1)
    }

    val filterArg = args(5)
    val filterQuery = new FilterQuery()
    if (filterType == "follow") {
      filterQuery.follow(filterArg.toLong)
    } else {
      filterQuery.track(filterArg)
    }

    val spark = SparkSession.builder().master("local[*]").appName("RawTweets").getOrCreate()
    val streamingContext = new StreamingContext(spark.sparkContext, Seconds(1))

    new TwitterInputDStream(streamingContext, None, Some(filterQuery), StorageLevel.MEMORY_AND_DISK_SER_2)
      .map(s => Tuple1(s))
      .foreachRDD(rdd => spark.createDataFrame(rdd).toDF("value")
        .write
        .format("kafka")
        .option("kafka.bootstrap.servers", args(6))
        .option("topic", s"stream-raw-$filterType-$filterArg")
        .save()
      )

    streamingContext.start()
    streamingContext.awaitTermination()
  }
}
