from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
import json


spark = SparkSession \
    .builder \
    .appName("ToRedis") \
    .config("spark.redis.host", "rbd-5") \
    .config("spark.redis.port", "7001") \
    .getOrCreate()


df = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", "rbd-4:9092, rbd-5:9092, rbd-6:9092") \
    .option("subscribe", "staging") \
    .option("startingOffsets", "earliest") \
    .load() \
    .selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)") \
    .withColumn("active", get_json_object(col("value"), "$.active")) \
    .where(col("active") == True) \
    .withColumn("adressed_at", get_json_object(col("value"), "$.adressed_at")) \
    .withColumn("tweet_id", get_json_object(col("value"), "$.tweet_id")) \
    .withColumn("emotion", get_json_object(col("value"), "$.emotion")) \
    .withColumn("afinn_sentiment", get_json_object(col("value"), "$.afinn_sentiment")) \
    .withColumn("date", get_json_object(col("value"), "$.date")) \
    .dropDuplicates(["tweet_id"])

result = df \
    .select("date", "adressed_at", "emotion") \
    .withColumn("method", lit("cnn")) \
    .withColumnRenamed("emotion", "sentiment") \
    .union(
        df
        .select("date", "adressed_at", "afinn_sentiment")
        .withColumn("method", lit("afinn"))
        .withColumnRenamed("afinn_sentiment", "sentiment")
    ) \
    .groupBy("date", "adressed_at", "method", "sentiment") \
    .count() \
    .withColumn("key", concat("date", lit("|"), "adressed_at", lit("|"), "method", lit("|"), "sentiment")) \
    .drop("date", "adressed_at", "method", "sentiment")


def write_dataframe(df,  epoch_id):
    df.write \
        .format("org.apache.spark.sql.redis") \
        .option("table", "visualization") \
        .option("key.column", "key") \
        .mode("append")\
        .save()


query = result \
    .writeStream \
    .outputMode("update") \
    .foreachBatch(write_dataframe) \
    .start()


query.awaitTermination()
