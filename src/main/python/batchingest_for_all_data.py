#TODO Make function and call 4 times function instead of repeating code
from kafka import KafkaProducer
from TwitterAPI import TwitterAPI
from datetime import datetime, timedelta
import json

today = datetime.now() - timedelta(days=1) #remove timedelta, when productive use in a cronjob
today = today.strftime('%Y%m%d') + "0000"
yesterday = datetime.now() - timedelta(days=2) #set days = 1, when productive use in a cronjob
yesterday = yesterday.strftime('%Y%m%d') + "0000"

#consumer_key = TODO
#consumer_secret = TODO
#access_token = TODO
#access_token_secret = TODO
api = TwitterAPI(consumer_key, consumer_secret, access_token, access_token_secret)

PRODUCT = '30day'
LABEL = 'rbdtest'

r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'trump', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100'})

for item in r:
    producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
    producer.send('batch-raw-track-trump', value=item)
    producer.flush()

#while "next" in r.json().keys(): => Use this insted of if in productive use (paid TwitterAPI)
if "next" in r.json().keys():
    r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'trump', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100', 'next':r.json()['next']})

    for item in r:
        producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
        producer.send('batch-raw-track-trump', value=item)
        producer.flush()


r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'sanders', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100'})

for item in r:
    producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
    producer.send('batch-raw-track-sanders', value=item)
    producer.flush()

#while "next" in r.json().keys(): => Use this insted of if in productive use (paid TwitterAPI)
if "next" in r.json().keys():
    r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'sanders', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100', 'next':r.json()['next']})

    for item in r:
        producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
        producer.send('batch-raw-track-sanders', value=item)
        producer.flush()


r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'from:25073877', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100'})

for item in r:
    producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
    producer.send('batch-raw-follow-25073877', value=item)
    producer.flush()

#while "next" in r.json().keys(): => Use this insted of if in productive use (paid TwitterAPI)
if "next" in r.json().keys():
    r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'from:25073877', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100', 'next':r.json()['next']})

    for item in r:
        producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
        producer.send('batch-raw-follow-25073877', value=item)
        producer.flush()


r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'from:216776631', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100'})

for item in r:
    producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
    producer.send('batch-raw-follow-216776631', value=item)
    producer.flush()

#while "next" in r.json().keys(): => Use this insted of if in productive use (paid TwitterAPI)
if "next" in r.json().keys():
    r = api.request('tweets/search/%s/:%s' % (PRODUCT, LABEL), {'query':'from:216776631', 'fromDate':yesterday, 'toDate':today, 'maxResults':'100', 'next':r.json()['next']})

    for item in r:
        producer = KafkaProducer(value_serializer=lambda v: json.dumps(v).encode('utf-8'), bootstrap_servers='rbd-4:9092, rbd-5:9092, rbd-6:9092', batch_size=0)
        producer.send('batch-raw-follow-216776631', value=item)
        producer.flush()