import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from datetime import datetime, timedelta

from rediscluster import RedisCluster
startup_nodes = [{"host": "rbd-4", "port": "7000"}]
r = RedisCluster(startup_nodes=startup_nodes, decode_responses=True)

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div(children=[
    html.H1(children='Shiny'),
    dcc.Tabs([
        dcc.Tab(label='Current', children=[
            dcc.Graph(
                id='afinn-trump-graph'
            ),
            dcc.Graph(
                id='afinn-sanders-graph'
            ),
            dcc.Graph(
                id='cnn-trump-graph'
            ),
            dcc.Graph(
                id='cnn-sanders-graph'
            ),
            dcc.Interval(
                id='interval-component',
                interval=1*1000,  # in milliseconds
                n_intervals=0
            )
        ]), dcc.Tab(label='History', children=[]),

    ])])

date = (datetime.today() - timedelta(hours=8)).strftime('%Y-%m-%d')

@app.callback([Output('afinn-trump-graph', 'figure'), Output('afinn-sanders-graph', 'figure'), Output('cnn-trump-graph', 'figure'), Output('cnn-sanders-graph', 'figure')],
              [Input('interval-component', 'n_intervals')])
def update(n):
    afinn_trump_values = []
    for i in ['-1', '0', '1']:
        value = r.hget(f'visualization:{date}|trump|afinn|{i}', 'count')
        if value:
            value = int(value)
        else:
            value = 0
        afinn_trump_values.append(value)

    afinn_sanders_values = []
    for i in ['-1', '0', '1']:
        value = r.hget(f'visualization:{date}|sanders|afinn|{i}', 'count')
        if value:
            value = int(value)
        else:
            value = 0
        afinn_sanders_values.append(value)

    cnn_trump_values = []
    for i in ['0.0', '1.0']:
        value = r.hget(f'visualization:{date}|trump|cnn|{i}', 'count')
        if value:
            value = int(value)
        else:
            value = 0
        cnn_trump_values.append(value)

    cnn_sanders_values = []
    for i in ['0.0', '1.0']:
        value = r.hget(f'visualization:{date}|sanders|cnn|{i}', 'count')
        if value:
            value = int(value)
        else:
            value = 0
        cnn_sanders_values.append(value)

    return {
        'data': [
            {
                'values': afinn_trump_values,
                'text': afinn_trump_values,
                'labels': ['negative', 'neutral', 'positive'],
                'type': 'pie'
            },
        ],
        'layout': {
            'title': 'AFINN Trump Sentiments'
        }
    }, {
        'data': [
            {
                'values': afinn_sanders_values,
                'text': afinn_sanders_values,
                'labels': ['negative', 'neutral', 'positive'],
                'type': 'pie'
            },
        ],
        'layout': {
            'title': 'AFINN Sanders Sentiments'
        }
    }, {
        'data': [
            {
                'values': cnn_trump_values,
                'text': cnn_trump_values,
                'labels': ['negative', 'positive'],
                'type': 'pie'
            },
        ],
        'layout': {
            'title': 'CNN Trump Sentiments'
        }
    }, {
        'data': [
            {
                'values': cnn_sanders_values,
                'text': cnn_sanders_values,
                'labels': ['negative', 'positive'],
                'type': 'pie'
            },
        ],
        'layout': {
            'title': 'CNN Sanders Sentiments'
        }
    }


if __name__ == '__main__':
    app.run_server(host='0.0.0.0', debug=False)
