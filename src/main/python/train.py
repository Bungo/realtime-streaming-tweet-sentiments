from pyspark.sql import SparkSession
from pyspark.ml.feature import HashingTF, IDF, Tokenizer
from pyspark.ml.feature import StringIndexer
from pyspark.ml import Pipeline
from pyspark.ml.feature import CountVectorizer
from pyspark.ml.classification import LogisticRegression
from pyspark.ml.evaluation import BinaryClassificationEvaluator


spark = SparkSession \
    .builder \
    .appName("train") \
    .getOrCreate()

train_set = spark \
    .read \
    .option("delimiter", "\t") \
    .option("header", True) \
    .option("inferSchema", True) \
    .csv('hdfs://rbd-7:9000/data/train.tsv') \
    .dropna() \
    .toDF("target", "text")

val_set = spark \
    .read \
    .option("delimiter", "\t") \
    .option("header", True) \
    .option("inferSchema", True) \
    .csv('hdfs://rbd-7:9000/data/val.tsv') \
    .dropna() \
    .toDF("target", "text")


tokenizer = Tokenizer(inputCol="text", outputCol="words")
cv = CountVectorizer(vocabSize=2**16, inputCol="words", outputCol='cv')
idf = IDF(inputCol='cv', outputCol="features", minDocFreq=5) #minDocFreq: remove sparse terms
label_stringIdx = StringIndexer(inputCol = "target", outputCol = "label")
lr = LogisticRegression(maxIter=100)
pipeline = Pipeline(stages=[tokenizer, cv, idf, label_stringIdx, lr])

pipelineFit = pipeline.fit(train_set)
predictions = pipelineFit.transform(val_set)
accuracy = predictions.filter(predictions.label == predictions.prediction).count() / float(val_set.count())
evaluator = BinaryClassificationEvaluator(rawPredictionCol="rawPrediction")
roc_auc = evaluator.evaluate(predictions)

print("Accuracy Score: {0:.4f}".format(accuracy))
print("ROC-AUC: {0:.4f}".format(roc_auc))

pipelineFit.write().overwrite().save("hdfs://rbd-7:9000/data/pipelineFit")