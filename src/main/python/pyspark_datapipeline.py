# TODO:
# - since we extract the retweets we may introduce duplicates -> deduplicate


from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.types import *
from pyspark.ml import PipelineModel
import json
import os
import sys

if len(sys.argv) < 3:
    raise Exception(
        "Please specify a param for loadtyp (stream or batch) and datapath in HDFS (where afin-Textfile and Modeldata is stored")
elif not isinstance(sys.argv[1], str) or (sys.argv[1].lower() != "stream" and sys.argv[1].lower() != "batch"):
    raise Exception(
        "Please specify a parameter 'stream' or 'batch' as first param")
elif not isinstance(sys.argv[2], str):
     raise Exception(
        "Please specify a parameter for datapath in HDFS")
        
apptype = sys.argv[1].lower()
datapath = sys.argv[2]
if not datapath.endswith("/"):
    datapath += "/"
if not datapath.startswith("hdfs://rbd-7:9000/"):
    datapath = "hdfs://rbd-7:9000/" + datapath

parse = udf(lambda s: json.loads(s, object_hook=json.dumps), ArrayType(StringType()))


def create_entity_df(entity, data):
    return entities.select("tweet_id", entity) \
        .withColumn(entity, explode(parse(col(entity)))) \
        .withColumn("type", lit(entity)) \
        .withColumn("data", get_json_object(col(entity), "$." + data)) \
        .drop(entity)

spark = SparkSession \
    .builder \
    .appName("staging " + apptype) \
    .getOrCreate()

    
afinn_path = datapath + 'AFINN-111.txt'
afinn = spark \
    .read \
    .option('delimiter', '\t') \
    .option('inferSchema', True) \
    .csv(afinn_path) \
    .toDF('word', 'score') \
    .persist()

df = spark \
    .readStream \
    .format("kafka") \
    .option("kafka.bootstrap.servers", "rbd-4:9092, rbd-5:9092, rbd-6:9092") \
    .option("subscribePattern", apptype + "-raw-.*") \
    .option("startingOffsets", "earliest") \
    .load() \
    .selectExpr("CAST(topic AS STRING)", "CAST(key AS STRING)", "CAST(value AS STRING)")

retweets = df.withColumn("value", get_json_object(col("value"), "$.retweeted_status")) \
    .dropna(subset="value") \
    .withColumn("active", lit(False))

quotes = df.withColumn("value", get_json_object(col("value"), "$.quoted_status")) \
    .dropna(subset="value") \
    .withColumn("active", lit(False))

tweet = df.withColumn("active", lit(True)) \
    .union(retweets).union(quotes) \
    .withColumn("text", get_json_object(col("value"), "$.text")) \
    .withColumn("full_text", get_json_object(col("value"), "$.extended_tweet.full_text")) \
    .withColumn("text", coalesce(col("full_text"), col("text"))) \
    .withColumn("tweet_id", get_json_object(col("value"), "$.id_str").cast("long")) \
    .drop("full_text")

pipelineModel = PipelineModel.load(datapath + "pipelineFit")

tweet = pipelineModel.transform(tweet).withColumn("author_id", get_json_object(col("value"), "$.user.id_str").cast("long")) \
    .withColumn("reply_id", get_json_object(col("value"), "$.in_reply_to_status_id_str").cast("long")) \
    .withColumn("quote_id", get_json_object(col("value"), "$.quoted_status.id_str").cast("long")) \
    .withColumn("retweet_id", get_json_object(col("value"), "$.retweeted_status.id_str").cast("long")) \
    .withColumn("timestamp", unix_timestamp(get_json_object(col("value"), "$.created_at"), "EEE MMM dd HH:mm:ss Z yyyy")) \
    .withColumn("source", get_json_object(col("value"), "$.source")) \
    .withColumn("retweet_count", get_json_object(col("value"), "$.retweet_count").cast(IntegerType())) \
    .withColumn("lang", get_json_object(col("value"), "$.lang")) \
    .withColumn("date", split(from_utc_timestamp(col("timestamp").cast(TimestampType()), "PST"), " ").getItem(0)) \
    .withColumn("adressed_at", when(col("topic") == apptype + "-raw-follow-25073877", "trump").when(col("topic") == apptype + "-raw-follow-216776631", "sanders").when(col("topic") == apptype + "-raw-track-trump", "trump").when(col("topic") == apptype + "-raw-track-sanders", "sanders").otherwise("other")) \
    .withColumnRenamed("prediction", "emotion") \
    .drop("topic", "key", "value", "full_text", "words", "cv", "features", "rawPrediction", "probability")


if apptype == "batch":
    user = df.withColumn("user_id", get_json_object(col("value"), "$.user.id_str").cast("long")) \
        .withColumn("name", get_json_object(col("value"), "$.user.name")) \
        .withColumn("screen_name", get_json_object(col("value"), "$.user.screen_name")) \
        .withColumn("location", get_json_object(col("value"), "$.user.location")) \
        .withColumn("timestamp", unix_timestamp(get_json_object(col("value"), "$.user.created_at"), "EEE MMM dd HH:mm:ss Z yyyy")) \
        .drop("key", "value")

    entities = df.withColumn("hashtags", get_json_object(col("value"), "$.entities.hashtags")) \
        .withColumn("tweet_id", get_json_object(col("value"), "$.id_str").cast("long")) \
        .withColumn("url", get_json_object(col("value"), "$.entities.urls")) \
        .withColumn("mention", get_json_object(col("value"), "$.entities.user_mentions")) \
        .withColumn("symbol", get_json_object(col("value"), "$.entities.symbols")) \
        .dropna(subset="url") \
        .drop("key", "value")

    media = df.withColumn("media", get_json_object(col("value"), "$.extended_entities.media")) \
        .withColumn("tweet_id", get_json_object(col("value"), "$.id_str").cast("long")) \
        .dropna(subset="media") \
        .drop("key", "value") \
        .withColumn("media", explode(parse(col("media")))) \
        .withColumn("type", get_json_object(col("media"), "$.type")) \
        .drop("key", "value")

    photos = media.filter(col("type") == "photo").select("tweet_id", "media", "type") \
        .withColumn("data", get_json_object(col("media"), "$.media_url")) \
        .drop("media")

    animated = media.filter(col("type") != "photo").select("tweet_id", "media", "type") \
        .withColumn("data", get_json_object(col("media"), "$.video_info")) \
        .drop("media")

    mentions = create_entity_df("mention", "id_str")
    urls = create_entity_df("url", "expanded_url")
    symbols = create_entity_df("symbol", "text")

    entity = photos.union(animated).union(mentions).union(urls).union(symbols)

    def wordlist(df, epoch_id):
        df = df.withColumn("word",  explode(split(col("text"), " "))) \
        .withColumn("word", lower(col("word"))) \
        .join(afinn, "word", "left") \
        .groupby(df.columns) \
        .agg(mean("score").alias("afinn_sentiment")) \
        .withColumn("afinn_sentiment", when(col("afinn_sentiment") > 0, 1).when(col("afinn_sentiment") < 0, -1).otherwise(0)) \
        
        df.write \
        .format("parquet") \
        .option("checkpointLocation", "hdfs://rbd-7:9000/sparkcheckpoints/tweets_processing") \
        .option("path", "hdfs://rbd-7:9000/datawarehouse/tweets") \
        .mode("append") \
        .partitionBy("date") \
        .save()


    tweet_query = tweet.writeStream \
        .outputMode("update") \
        .foreachBatch(wordlist) \
        .start()

    user_query = user.writeStream \
        .format("parquet") \
        .option("checkpointLocation", "hdfs://rbd-7:9000/sparkcheckpoints/user_processing") \
        .option("path", "hdfs://rbd-7:9000/datawarehouse/users") \
        .outputMode("append") \
        .start()

    entity_query = entity.writeStream \
        .format("parquet") \
        .option("checkpointLocation", "hdfs://rbd-7:9000/sparkcheckpoints/entity_processing") \
        .option("path", "hdfs://rbd-7:9000/datawarehouse/entities") \
        .outputMode("append") \
        .start()

    entity_query.awaitTermination()
    user_query.awaitTermination()

else:
    tweet = tweet \
        .withColumn("word",  explode(split(col("text"), " "))) \
        .withColumn("word", lower(col("word"))) \
        .join(afinn, "word", "left") \
        .groupby(tweet.columns) \
        .agg(mean("score").alias("afinn_sentiment")) \
        .withColumn("afinn_sentiment", when(col("afinn_sentiment") > 0, 1).when(col("afinn_sentiment") < 0, -1).otherwise(0))

    tweet_query = tweet \
        .selectExpr("CAST(tweet_id AS STRING) AS key", "to_json(struct(*)) AS value") \
        .writeStream \
        .format("kafka") \
        .outputMode("update") \
        .option("kafka.bootstrap.servers", "rbd-4:9092,rbd-5:9092,rbd-6:9092") \
        .option("topic", "staging") \
        .option("checkpointLocation", "hdfs://rbd-7:9000/sparkcheckpoints/stream_processing") \
        .start()

tweet_query.awaitTermination()
