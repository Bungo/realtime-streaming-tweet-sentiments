import json
import pandas as pd
import time

from kafka import KafkaProducer

# eventgenerator
class EventGenerator:
    def __init__(self, df, eventtime_column, speed):
        self.df = df
        self.eventtime_column = eventtime_column
        self.speed = speed
        self.df = self.df.sort_values(by=self.eventtime_column, ascending=True)
        self.last_time = self.df[self.eventtime_column].min()

    def generate_events(self):
        for i in self.df.index:
            row = df.loc[i]
            event = row.to_json()
            eventtime = row[eventtime_column]
            time.sleep(
                (eventtime - self.last_time).total_seconds() / self.speed)
            self.last_time = eventtime
            yield event


# args
bootstrap_servers = '86.119.25.210:9092, 86.119.25.196:9092, 86.119.25.219:9092'
#topic = 'stream-raw-follow-216776631'
#topic = 'stream-raw-follow-25073877'
#topic = 'stream-raw-track-trump'
topic = 'stream-raw-track-sanderis'
speed = 1.0
eventtime_column = 'created_at'
#input_path = '../../../testdata/samples/2020-02-16/data_216776631.json'
#input_path = '../../../testdata/samples/2020-02-16/data_25073877.json'
#input_path = '../../../testdata/samples/2020-02-16/data_trump.json'
input_path = '../../../testdata/samples/2020-02-16/data_sanders.json'


# use eventgenerator and write to kafka
producer = KafkaProducer(bootstrap_servers=bootstrap_servers, batch_size=0)
df = pd.read_json(input_path, lines=True)
generator = EventGenerator(df, eventtime_column, speed)
for i in generator.generate_events():
    producer.send(topic, i.encode('utf-8'))
producer.flush()